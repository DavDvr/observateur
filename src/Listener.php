<?php

namespace Event;

class Listener
{
    /**
     * @var callable
     */
    public $callback;

    /**
     * Définit si le listener peut être appelé plusieurs fois
     * @var bool
     */
    private bool $once = false;

    /**
     * Permet de savoir le nbr de fois que le listener a été appelé
     * @var int
     */
    private int $calls= 0;

    /**
     * @var int
     */
    public int $priority;

    /**
     * Permet de stopper les évènements parents
     * @var bool
     */
    public bool $stopPropagation= false;

    /**
     * Listener constructor.
     * @param callable $callback
     * @param int $priority
     */
    public function __construct(callable $callback, int $priority)
    {
        $this->callback = $callback;
        $this->priority = $priority;
    }

    /**
     * Permet d'utiliser cette methode dans ma classe Emitter
     * afin d'utiliser la methode call_user_func_array
     * Je verifie si le listener ne peux pas être appelé plusieurs fois et que le nbr de calls est supérieur à 0
     * si oui je retourne null
     * @param array $args
     * @return false|mixed
     */
    public function handle(array $args)
    {
        if ($this->once && $this->calls > 0)
        {
            return null;
        }
        $this->calls++;
        return call_user_func_array($this->callback, $args);
    }

    /**
     * Permet de dire que l'on veut que le listener soit appelé une fois
     * @return $this
     */
    public function once(): Listener
    {
        $this->once = true;
        return $this;
    }

    /**
     * Permet d'arreter l'exécution des évènements suivants
     * @return $this
     */
    public function stopPropagation(): Listener
    {
        $this->stopPropagation = true;
        return $this;
    }
}
