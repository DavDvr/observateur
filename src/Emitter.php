<?php


namespace Event;


class Emitter
{
    /**
     * @var $_instance
     */
    private static $_instance;

    /**
     * Sauvegarde la liste des écouteurs
     * @var Listener [][]
     */
    private  $listeners = [];

    /**
     * return toujours la même instance (singleton)
     * @return Emitter
     */
    public static function getInstance(): Emitter{

        if (!self::$_instance){

            self::$_instance = new self();
        }
            return self::$_instance;
    }

    /**
     * Envoie un évènement
     * @param string $event
     * @param mixed ...$args
     */
    public function emit(string $event, ...$args)
    {
       if($this->hasListener($event)){
            foreach ($this->listeners[$event] as $listener)
            {
                $listener->handle($args);
                if ($listener->stopPropagation)
                {
                    break;
                }
            }
       }
    }

    /**
     * Permet d'écouter un évènement
     * @param string $event
     * @param callable $callable
     * @param int $priority
     * @return Listener
     * @throws DoubleEventException
     */
    public function on(string $event, callable $callable, int $priority = 0): Listener
    {
        if (!$this->hasListener($event)){

            $this->listeners[$event] = [];
        }
        $this->checkCallableForSameEvent($event, $callable);
        $listener = new Listener($callable, $priority);
        $this->listeners[$event][] = $listener;

        //je réorganise les listeners
        $this->sortListeners($event);

        return $listener;
    }

    /**
     * Permet d'écouter un évènement une seule fois et de lancer le listener une seule fois
     * @param string $event
     * @param callable $callable
     * @param int $priority
     * @return Listener
     * @throws DoubleEventException
     */
    public function once(string $event, callable $callable, int $priority = 0): Listener
    {
        return $this->on($event, $callable, $priority)->once();
    }

    /**
     * Permet d'ajouter un subscriber qui va écouter plusieurs évènements
     * @param SubscriberInterface $subscriber
     * @throws DoubleEventException
     */
    public function addSubscriber(SubscriberInterface $subscriber)
    {
        $events = $subscriber->getEvents();
        foreach ($events as $event => $method) {
            $this->on($event, [$subscriber, $method]);
        }
    }

    /**
     * Permet de verifier si un event existe dans mon tableau listeners
     * @param string $event
     * @return bool
     */
    private function hasListener(string $event): bool
    {
        return array_key_exists($event, $this->listeners);
    }

    /**
     * @param $event
     * Permet de definir les priorités
     * Il renvoie -1, 0 ou 1 lorsque le premier élément est respectivement
     * inférieur, égal ou supérieur au deuxième élément.
     */
    private function sortListeners($event)
    {
        uasort($this->listeners[$event], function ($firstElement, $secondElement):int
        {
            return $firstElement->priority < $secondElement->priority;
        });
    }

    private function checkCallableForSameEvent(string $event, callable $callable): bool
    {
        foreach ($this->listeners[$event] as $listener){

            if ($listener->callback === $callable){
                throw new DoubleEventException();
            }
        }
        return false;
    }

}
