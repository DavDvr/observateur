<?php
//test de la class Emitter
use Event\Emitter;
use Kahlan\Plugin\Double;

class FakeSubscriber implements \Event\SubscriberInterface{

    public function getEvents(): array
    {
        return [
            'Comment.created' => 'onNewComment',
            'Post.created' => 'onNewPost',
        ];
    }
}


describe(Emitter::class, function ()
{
    /**
     * Fonction permettant de modifier avant chaque test le singleton
     * afin d'éviter les conflits entre mes tests
     * Ceci est possible grace à la reflection de ma class
     */
    beforeEach(function (){
        $reflection = new ReflectionClass(Emitter::class);
        $instance = $reflection->getProperty('_instance');
        $instance->setAccessible(true);
        $instance->setValue(null, null);
        $instance->setAccessible(false);
    });

    /**
     * Cette fonction permet d'accéder à mon instance emitter en faisant un $this->emitter
     */
    given('emitter', function (){return Emitter::getInstance();});

    /**
     * je verifie si l'instance est une instance de Emitter
     * Je verifie ensuite si l'instance est identique a Emitter::getInstance()
     */
    it('should be a singleton', function ()
    {
        $instance = Emitter::getInstance();
        expect($instance)->toBeAnInstanceOf(Emitter::class);
        expect($instance)->toBe(Emitter::getInstance());
    });

    /**
     * Test à l'intérieur sont des tests de la methode ::on
     */
    describe('::on', function (){

        it('should trigger listened event', function (){

            /**
             * je créer une fausse instance de listener
             */
            $listener = Double::instance();

            $comment = ['name'=> 'Devaure'];

            /**
             * je m'attend à ce que tu reçois une fois la methode onNewComment
             * avec le paramètre $comment
             */
            expect($listener)->toReceive('onNewComment')->once()->with($comment);

            /**
             * je m'attend à ce que tu reçois deux fois la methode onNewComment
             * avec le paramètre $comment
             */
            //expect($listener)->toReceive('onNewComment')->times(2)->with($comment);

            /**
             * j'écoute l'évènement Comment.created et passe en second param un tableau
             * qui contient en premier le faux listener et en second param la methode
             */
            $this->emitter->on('Comment.created', [$listener, 'onNewComment']);

            /**
             * j'envoie l'évènement
             */
            $this->emitter->emit('Comment.created', $comment);
            //$this->emitter->emit('Comment.created', $comment);

        });

        it('should trigger events in the right order', function (){

            $listener = Double::instance();
            expect($listener)->toReceive('onNewComment2')->once()->ordered;
            expect($listener)->toReceive('onNewComment')->once()->ordered;
            $this->emitter->on('Comment.created', [$listener, 'onNewComment'], 1);
            $this->emitter->on('Comment.created', [$listener, 'onNewComment2'], 200);
            $this->emitter->emit('Comment.created');
        });

        it('should run the first event first', function (){

            $listener = Double::instance();
            expect($listener)->toReceive('onNewComment')->once()->ordered;
            expect($listener)->toReceive('onNewComment2')->once()->ordered;
            $this->emitter->on('Comment.created', [$listener, 'onNewComment']);
            $this->emitter->on('Comment.created', [$listener, 'onNewComment2']);
            $this->emitter->emit('Comment.created');
        });

        it('should prevent the same listener twice', function (){

            $listener = Double::instance();


            $closure = function () use ($listener){
                $this->emitter->on('Comment.created', [$listener, 'onNewComment']);
                $this->emitter->on('Comment.created', [$listener, 'onNewComment']);
            };

            expect($closure)->toThrow(new \Event\DoubleEventException());

        });

    });

    describe('::once', function (){

        it('should trigger events once', function (){

            $listener = Double::instance();

            $comment = ['name'=> 'Devaure'];

            expect($listener)->toReceive('onNewComment')->once()->with($comment);

            $this->emitter->on('Comment.created', [$listener, 'onNewComment'])->once();

            /**
             * j'envoie l'évènement
             */
            $this->emitter->emit('Comment.created', $comment);
            $this->emitter->emit('Comment.created', $comment);

        });

    });

    describe('::stopPropagation', function (){

       it('should stop next listener', function (){

           $listener = Double::instance();
           expect($listener)->toReceive('onNewComment')->once();
           expect($listener)->not->toReceive('onNewComment2')->once();

           $this->emitter->on('Comment.created', [$listener, 'onNewComment'])->stopPropagation();
           $this->emitter->on('Comment.created', [$listener, 'onNewComment2']);
           $this->emitter->emit('Comment.created');
       });

    });

    describe('::addSubscriber', function (){

        it('should trigger every events', function (){

            $subscriber = Double::instance([
                'extends' => FakeSubscriber::class,
                'methods' => ['onNewComment','onNewPost']
            ]);

            $comment = ['name'=> 'Devaure'];

            expect($subscriber)->toReceive('onNewComment')->times(2)->with($comment);
            expect($subscriber)->toReceive('onNewPost')->once()->with(20,400,500);

            $this->emitter->addSubscriber($subscriber);

            $this->emitter->emit('Comment.created', $comment);
            $this->emitter->emit('Comment.created', $comment);
            $this->emitter->emit('Post.created', 20,400,500);

        });
    });

});