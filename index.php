<?php

use Event\Emitter;

require 'vendor/autoload.php';

//je créer l'instance
$emitter = Emitter::getInstance();

//j'envoie un évènement
$emitter->emit('Comment.new', 'David', 'Devaure');

//j'ecoute un évènement
$emitter->on('Comment.new', function ($firstname, $lastname){
    echo $firstname .' '. $lastname . ' à posté un nouveau commentaire !';
}, 1);

//j'ecoute un évènement
$emitter->on('Comment.new', function ($firstname, $lastname){
    echo $firstname .' '. $lastname . ' à posté un nouveau commentaire !';
},100);
